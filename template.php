<?php
  function amtenergo_menu_link(array $variables) {
    $element = $variables['element'];
    $sub_menu = '';

    if ($element['#below']) {
      $sub_menu = drupal_render($element['#below']);
    }
    $link = '<p class="menu-link">' . l($element['#title'], $element['#href'], $element['#localized_options']) . '</p>';
    $desc = isset($element['#localized_options']['attributes']['title']) ? '<p class="menu-desc">' . $element['#localized_options']['attributes']['title'] . '</p>' : "";
    return '<li' . drupal_attributes($element['#attributes']) . '><div class="menu-link-desc">' . $link . $desc . '</div>' . $sub_menu . "</li>\n";
  }

  function amtenergo_preprocess_html(&$vars) {

//    // use the $html5shiv variable in their html.tpl.php
//    $element = array(
//      'element' => array(
//        '#tag' => 'script',
//        '#value' => '',
//        '#attributes' => array(
//          'src' => '//html5shiv.googlecode.com/svn/trunk/html5.js',
//        ),
//      ),
//    );
//
//    $shimset = theme_get_setting('boron_shim');
//    $script = theme('html_tag', $element);
//    //If the theme setting for adding the html5shim is checked, set the variable.
//    if ($shimset == 1) { $vars['html5shim'] = "\n<!--[if lt IE 9]>\n" . $script . "<![endif]-->\n"; }

    $shims = array();

    if (theme_get_setting('boron_shim'))
      $shims[] = '//cdnjs.cloudflare.com/ajax/libs/html5shiv/3.6.2/html5shiv.js';
    if (theme_get_setting('nwmatcher'))
      $shims[] = '//s3.amazonaws.com/nwapi/nwmatcher/nwmatcher-1.2.5-min.js';
    if (theme_get_setting('selectivizr'))
      $shims[] = '//cdnjs.cloudflare.com/ajax/libs/selectivizr/1.0.2/selectivizr-min.js';
    if (theme_get_setting('respond'))
      $shims[] = '//cdnjs.cloudflare.com/ajax/libs/respond.js/1.1.0/respond.min.js';
    if (theme_get_setting('ie9'))
      $shims[] = '//ie7-js.googlecode.com/svn/version/2.1(beta4)/IE9.js';

    if (count($shims) > 0) {
      $shim_output = "";

      foreach ($shims as $shim_number => $shim) {

        $element = array(
          'element' => array(
            '#tag' => 'script',
            '#value' => '',
            '#attributes' => array(
              'src' => $shim,
            ),
          ),
        );

        $shim_output .= theme('html_tag', $element);
      }
      $vars['shims'] = $shim_output;
    }
  }