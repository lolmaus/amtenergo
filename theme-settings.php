<?php
function amtenergo_form_system_theme_settings_alter(&$form, &$form_state) {

  $form['libraries']['#title'] = t('Libraries for IE < 9');

  $form['libraries']['nwmatcher'] = array(
    '#type'          => 'checkbox',
    '#title'         => t('NWMatcher'),
    '#default_value' => theme_get_setting('nwmatcher'),
    '#description'   => t('Required by Selectivizr.')
  );
  $form['libraries']['selectivizr'] = array(
    '#type'          => 'checkbox',
    '#title'         => t('Selectivizr'),
    '#default_value' => theme_get_setting('selectivizr'),
    '#description'   => t('CSS3 pseudo-class and attribute selectors for IE 6-8.')
  );
  $form['libraries']['respond'] = array(
    '#type'          => 'checkbox',
    '#title'         => t('Respond'),
    '#default_value' => theme_get_setting('respond'),
    '#description'   => t('A fast & lightweight polyfill for min/max-width CSS3 Media Queries (for IE 6-8, and more).')
  );
  $form['libraries']['ie9'] = array(
    '#type'          => 'checkbox',
    '#title'         => t('IE9.js'),
    '#default_value' => theme_get_setting('ie9'),
    '#description'   => t('Upgrade MSIE5.5-8 to be compatible with modern browsers.')
  );
}